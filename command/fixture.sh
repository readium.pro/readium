#!/usr/bin/env bash
for var in "Task" "LessonTask" "UserLesson" "User" "UserTask" "TaskData" "ReadSpeed" "UserReadSpeed" "Lesson" "Subscription";
do
   yes | php yii fixture $var
done
