<?php

use yii\db\Migration;

/**
 * Class m210113_145359_create_table_lesson
 */
class m210113_145359_create_table_lesson extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('lesson',[
            'id' => $this->primaryKey(),
            'number' => $this->integer()->notNull()->unique(),
            'status' => $this->integer()->notNull()
        ]);
        $this->addForeignKey(
            'fk-user_lesson-lesson_id',
            'user_lesson',
            'lesson_id',
            'lesson',
            'id'
        );
        $this->addForeignKey(
            'fk-lesson_task-lesson_id',
            'lesson_task',
            'lesson_id',
            'lesson',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-user_lesson-lesson_id',
            'user_lesson'
        );
        $this->dropForeignKey(
            'fk-lesson_task-lesson_id',
            'lesson_task'
        );
        $this->dropTable('lesson');
    }
}
