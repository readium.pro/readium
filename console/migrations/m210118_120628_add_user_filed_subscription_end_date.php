<?php

use yii\db\Migration;

/**
 * Class m210118_120628_add_user_filed_subscription_end_date
 */
class m210118_120628_add_user_filed_subscription_end_date extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            'user',
            'subscription_end_date',
            $this->dateTime()->defaultValue(null)->comment('Дата окончания подписки')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'subscription_end_date');
    }
}
