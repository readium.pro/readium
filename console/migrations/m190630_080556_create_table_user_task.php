<?php

use yii\db\Migration;

/**
 * Class m190630_080556_rbac
 */
class m190630_080556_create_table_user_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_task', [
            'id' => $this->primaryKey()->unique(),
            'user_lesson_id' => $this->integer()->notNull(),
            'task_id' => $this->integer()->notNull()->comment('id занятия'),
            'status' => $this->integer()->notNull()->comment('Статус'),
            'created_at' => $this->timestamp()->notNull()->comment('Дата добавления пользователю лекции'),
            'updated_at' => $this->time()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_task');
    }
}
