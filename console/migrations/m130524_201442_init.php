<?php

use yii\db\Migration;
use common\models\User;

class m130524_201442_init extends Migration
{
    public function up()
    {
        Yii::$app->db->createCommand('ALTER DATABASE `readium` COLLATE utf8_general_ci')->execute();
        $this->createTable('user', [
            'id' => $this->primaryKey()->unique(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),

            'status' => $this->smallInteger()->notNull()->defaultValue(User::STATUS_INACTIVE),
            'created_at' => 'datetime DEFAULT NOW()',
            'updated_at' => $this->timestamp()->notNull()
        ]);
    }

    public function down()
    {
        $this->dropTable('user');
    }
}
