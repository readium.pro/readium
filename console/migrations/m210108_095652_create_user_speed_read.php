<?php

use yii\db\Migration;

/**
 * Class m210108_095652_create_user_speed_read
 */
class m210108_095652_create_user_speed_read extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_read_speed', [
            'id' => $this->primaryKey()->notNull()->unique(),
            'user_id' => $this->integer()->notNull(),
            'words_per_minute' => $this->integer()->notNull(),
            'created_at' => $this->dateTime()->defaultExpression('NOW()')->notNull(),
        ]);
        $this->addForeignKey(
            'fk-user_read_speed-user_id',
            'user_read_speed',
            'user_id',
            'user',
            'id'
        );
        $this->createIndex(
            'idx-user_read_speed-user_id',
            'user_read_speed',
            'user_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-user_read_speed-user_id',
            'user_read_speed'
        );
        $this->dropIndex(
            'idx-user_read_speed-user_id',
            'user_read_speed'
        );
        $this->dropTable('user_read_speed');
    }
}
