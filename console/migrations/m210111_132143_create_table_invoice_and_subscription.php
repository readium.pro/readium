<?php

use yii\db\Migration;

/**
 * Class m210111_132143_create_table_invoice
 */
class m210111_132143_create_table_invoice_and_subscription extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('subscription',[
            'id' => $this->primaryKey()->notNull(),
            'price' => $this->float()->notNull()->comment('Цена подписки'),
            'count_day' => $this->integer()->notNull()->comment('Колличесвто дней'),
            'description' => $this->string()->notNull()->comment('Описание'),
            'created_at' => $this->dateTime()->defaultExpression('NOW()')->notNull()->comment('Дата создания'),
        ]);
        $this->addCommentOnTable('subscription','Подписка');
        $this->createTable('invoice',[
            'id' => $this->primaryKey()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull()->comment('Статус'),
            'amount' => $this->double()->notNull()->comment('Сумма'),
            'description' => $this->string('1024')->comment('Описание'),
            'created_at' => $this->dateTime()->defaultExpression('NOW()')->notNull(),
            'paid_at' => $this->dateTime()->comment('Дата оплаты'),
        ]);
        $this->addCommentOnTable('invoice','Счет-фактура');
        $this->createTable('subscription_invoice',[
            'subscription_id' => $this->integer()->notNull(),
            'invoice_id' => $this->integer()->notNull()
        ]);
        //Foreign key
        $this->addForeignKey(
            'fk-invoice-user_id',
            'invoice',
            'user_id',
            'user',
            'id'
        );
        $this->addForeignKey(
            'fk-subscription_invoice-subscription_id',
            'subscription_invoice',
            'subscription_id',
            'subscription',
            'id'
        );
        $this->addForeignKey(
            'fk-subscription_invoice-invoice_id',
            'subscription_invoice',
            'invoice_id',
            'invoice',
            'id'
        );

        //Index

        $this->createIndex(
            'idx-invoice-user_id',
            'invoice',
            'user_id'
        );
        $this->createIndex(
            'idx-subscription_invoice-invoice_id',
            'subscription_invoice',
            'invoice_id'
        );
        $this->createIndex(
            'idx-subscription_invoice-subscription_id',
            'subscription_invoice',
            'subscription_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-invoice-user_id',
            'invoice'
        );
        $this->dropForeignKey(
            'fk-subscription_invoice-subscription_id',
            'subscription_invoice'
        );
        $this->dropForeignKey(
            'fk-subscription_invoice-invoice_id',
            'subscription_invoice'
        );
        $this->dropIndex(
            'idx-invoice-user_id',
            'invoice'
        );
        $this->dropIndex(
            'idx-subscription_invoice-invoice_id',
            'subscription_invoice'
        );
        $this->dropIndex(
            'idx-subscription_invoice-subscription_id',
            'subscription_invoice'
        );

        $this->dropTable('subscription_invoice');
        $this->dropTable('subscription');
        $this->dropTable('invoice');
    }
}
