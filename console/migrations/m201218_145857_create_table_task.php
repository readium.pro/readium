<?php

use yii\db\Migration;

/**
 * Class m201218_145857_create_table_exercise
 */
class m201218_145857_create_table_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('task', [
            'id' => $this->primaryKey()->notNull()->unique(),
            'type_skill' => $this->integer()->notNull()->comment('Тип навыка'),
            'type_form' => $this->integer()->notNull()->comment('Тип формы'),
            'title' => $this->string(255)->notNull()->comment('Заголовок'),
            'description' => $this->string('2048')->notNull()->comment('Описание')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('task');
    }
}
