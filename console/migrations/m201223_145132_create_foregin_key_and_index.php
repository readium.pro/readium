<?php

use yii\db\Migration;

/**
 * Class m201223_145132_create_foregin_key_and_index
 */
class m201223_145132_create_foregin_key_and_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-user_lesson-user_id',
            'user_lesson',
            'user_id',
            'user',
            'id'
        );
        $this->addForeignKey(
            'fk-user_task-user_lesson_id',
            'user_task',
            'user_lesson_id',
            'user_lesson',
            'id'
        );
        $this->addForeignKey(
            'fk-user_task-task_id',
            'user_task',
            'task_id',
            'task',
            'id'
        );
        $this->addForeignKey(
            'fk-lesson_task-task_id',
            'lesson_task',
            'task_id',
            'task',
            'id'
        );
        $this->addForeignKey(
            'fk-task_data-task_id',
            'task_data',
            'task_id',
            'task',
            'id'
        );

        ///////////////
        $this->createIndex(
            'idx-user-id',
            'user',
            'id'
        );
        $this->createIndex(
            'idx-user_lesson-user_id',
            'user_lesson',
            'user_id'
        );

        ///

        $this->createIndex(
            'idx-user_task-task_id',
            'user_task',
            'task_id'
        );
        ////

        $this->createIndex(
            'idx-lesson_task-task_id',
            'lesson_task',
            'task_id'
        );
        $this->createIndex(
            'idx-task-id',
            'task',
            'id'
        );
        $this->createIndex(
            'idx-task_data-task_id',
            'task_data',
            'task_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-user_lesson-user_id',
            'user_lesson'
        );
        $this->dropForeignKey(
            'fk-user_task-user_lesson_id',
            'user_task'
        );
        $this->dropForeignKey(
            'fk-user_task-task_id',
            'user_task'
        );
        $this->dropForeignKey(
            'fk-lesson_task-task_id',
            'lesson_task'
        );
        $this->dropForeignKey(
            'fk-task_data-task_id',
            'task_data'
        );
        //////////////////
        $this->dropIndex(
            'idx-user-id',
            'user'
        );
        $this->dropIndex(
            'idx-user_lesson-user_id',
            'user_lesson'
        );

        ///

        $this->dropIndex(
            'idx-user_task-task_id',
            'user_task'
        );
        ////

        $this->dropIndex(
            'idx-lesson_task-task_id',
            'lesson_task'
        );
        $this->dropIndex(
            'idx-task-id',
            'task'
        );
        $this->dropIndex(
            'idx-task_data-task_id',
            'task_data'
        );
    }
}
