<?php

use yii\db\Migration;

/**
 * Class m210108_100836_create_read_speed
 */
class m210108_095500_create_read_speed extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('read_speed', [
            'id' => $this->primaryKey()->notNull()->unique(),
            'text' => $this->text()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('read_speed');
    }
}
