<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lesson`.
 */
class m190602_064250_create_lesson_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('lesson_task', [
            'id' => $this->primaryKey(),
            'lesson_id' => $this->integer()->notNull(),
            'task_id' => $this->integer()->notNull(),
            'created_at' => 'datetime DEFAULT NOW()',
            'updated_at' => $this->timestamp()->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('lesson');
    }
}
