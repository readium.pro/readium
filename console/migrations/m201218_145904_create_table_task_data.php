<?php

use yii\db\Migration;

/**
 * Class m201218_145904_create_table_exercise_data
 */
class m201218_145904_create_table_task_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('task_data', [
            'id' => $this->primaryKey()->notNull(),
            'task_id' => $this->integer()->notNull(),
            'data' => $this->text()->notNull()->comment('Данные'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('task_data');
    }
}
