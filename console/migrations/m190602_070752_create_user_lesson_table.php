<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_lesson`.
 */
class m190602_070752_create_user_lesson_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_lesson', [
            'id' => $this->primaryKey()->unique(),
            'user_id' => $this->integer()->notNull()->comment('id пользователя'),
            'lesson_id' => $this->integer()->notNull()->comment('Номмер лекции'),
            'status' => $this->integer()->notNull()->comment('Статус'),
            'created_at' => $this->timestamp()->notNull()->comment('Дата добавления пользователю лекции'),
            'updated_at' => $this->time()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_lesson');
    }
}
