<?php

namespace console\controllers;

use yii\console\Controller;
use Yii;

class DevController extends Controller
{
    /**
     * Удалит и заново создаст базу
     */
    public function actionDropDataBase()
    {
        $sql = sprintf('drop database %s;', getenv('MYSQL_DATABASE'));
        if (Yii::$app->getDb()->createCommand($sql)->execute()) {
            echo 'База успешно удалена!';
        } else {
            echo 'Не удалось удалить базу!';
        }
        echo PHP_EOL;

        $sql = sprintf('create database %s;', getenv('MYSQL_DATABASE'));
        if (Yii::$app->getDb()->createCommand($sql)->execute()) {
            echo 'База успешно созданна!';
        } else {
            echo 'Не удалось создать базу!';
        }
        echo PHP_EOL;
    }
}
