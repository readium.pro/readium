<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

class TaskDataFixture extends ActiveFixture
{
    public $modelClass = 'common\models\TaskData';
    public $dataFile = '@common/tests/_data/task_data.php';
}
