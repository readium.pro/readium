<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

class UserTaskFixture extends ActiveFixture
{
    public $modelClass = 'common\models\UserTask';
    public $dataFile = '@common/tests/_data/user_task.php';
}
