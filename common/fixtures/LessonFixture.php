<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

class LessonFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Lesson';
    public $dataFile = '@common/tests/_data/lesson.php';
}
