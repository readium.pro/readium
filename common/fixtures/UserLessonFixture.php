<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

class UserLessonFixture extends ActiveFixture
{
    public $modelClass = 'common\models\UserLesson';
    public $dataFile = '@common/tests/_data/user_lesson.php';
}
