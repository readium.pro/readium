<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

class UserReadSpeedFixture extends ActiveFixture
{
    public $modelClass = 'common\models\UserReadSpeed';
    public $dataFile = '@common/tests/_data/user_read_speed.php';
}
