<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

class SubscriptionFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Subscription';
    public $dataFile = '@common/tests/_data/subscription.php';
}
