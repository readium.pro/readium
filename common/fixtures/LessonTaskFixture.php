<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

class LessonTaskFixture extends ActiveFixture
{
    public $modelClass = 'common\models\LessonTask';
    public $dataFile = '@common/tests/_data/lesson_task.php';
}
