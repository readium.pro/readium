<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

class ReadSpeedFixture extends ActiveFixture
{
    public $modelClass = 'common\models\ReadSpeed';
    public $dataFile = '@common/tests/_data/read_speed.php';
}
