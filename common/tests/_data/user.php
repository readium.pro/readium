<?php

use common\models\User;

return [
    [
        'username' => 'user1',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ1',
        'password_hash' => Yii::$app->security->generatePasswordHash('123456az'),
        'email' => 'user1@gmail.com',
        'status' => User::STATUS_ACTIVE,
        'created_at' => '2019-06-04 15:44:45',
        'updated_at' => '2019-06-04 15:44:45'
    ],
    [
        'username' => 'user2',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ2',
        'password_hash' => Yii::$app->security->generatePasswordHash('123456az'),
        'email' => 'user2@gmail.com',
        'status' => User::STATUS_ACTIVE,
        'created_at' => '2019-06-04 15:44:45',
        'updated_at' => '2019-06-04 15:44:45'
    ]
];
