<?php

use common\models\UserLesson;

return [
    [
        'user_id' => 1,
        'lesson_id' =>  1,
        'status' =>  UserLesson::STATUS_COMPLETE,
    ],
    [
        'user_id' => 1,
        'lesson_id' => 2,
        'status' =>  UserLesson::STATUS_PROGRESS
    ],
    [
        'user_id' => 1,
        'lesson_id' => 3,
        'status' => UserLesson::STATUS_CREATE
    ],
    [
        'user_id' => 1,
        'lesson_id' => 4,
        'status' => UserLesson::STATUS_CREATE
    ],
];
