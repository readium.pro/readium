<?php

use common\models\Lesson;

return [
    [
        'number' => 1,
        'status' => Lesson::STATUS_FREE,
    ],
    [
        'number' => 2,
        'status' => Lesson::STATUS_FREE
    ],
    [
        'number' => 3,
        'status' => Lesson::STATUS_FREE
    ],
    [
        'number' => 4,
        'status' => Lesson::STATUS_PAYMENT
    ],
];
