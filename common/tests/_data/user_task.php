<?php

use common\models\UserTask;

return [
    [
        'user_lesson_id' => 1,
        'status' => UserTask::STATUS_COMPLETE,
        'task_id' => 1
    ],
    [
        'user_lesson_id' => 1,
        'status' => UserTask::STATUS_COMPLETE,
        'task_id' => 2
    ],
    [
        'user_lesson_id' => 1,
        'status' => UserTask::STATUS_COMPLETE,
        'task_id' => 3
    ],
    [
        'user_lesson_id' => 1,
        'status' => UserTask::STATUS_COMPLETE,
        'task_id' => 4
    ],
    [
        'user_lesson_id' => 1,
        'status' => UserTask::STATUS_COMPLETE,
        'task_id' => 5
    ],
    /////
    [
        'user_lesson_id' => 2,
        'status' => UserTask::STATUS_COMPLETE,
        'task_id' => 1
    ],
    [
        'user_lesson_id' => 2,
        'status' => UserTask::STATUS_COMPLETE,
        'task_id' => 2
    ],
    [
        'user_lesson_id' => 2,
        'status' => UserTask::STATUS_CREATE,
        'task_id' => 3
    ],
    [
        'user_lesson_id' => 2,
        'status' => UserTask::STATUS_CREATE,
        'task_id' => 4
    ],
    [
        'user_lesson_id' => 2,
        'status' => UserTask::STATUS_CREATE,
        'task_id' => 5
    ],
    /////
    [
        'user_lesson_id' => 3,
        'status' => UserTask::STATUS_CREATE,
        'task_id' => 1
    ],
    [
        'user_lesson_id' => 3,
        'status' => UserTask::STATUS_CREATE,
        'task_id' => 2
    ],
    [
        'user_lesson_id' => 3,
        'status' => UserTask::STATUS_CREATE,
        'task_id' => 3
    ],
    [
        'user_lesson_id' => 3,
        'status' => UserTask::STATUS_CREATE,
        'task_id' => 4
    ],
    [
        'user_lesson_id' => 3,
        'status' => UserTask::STATUS_CREATE,
        'task_id' => 5
    ],
    /////
    [
        'user_lesson_id' => 4,
        'status' => UserTask::STATUS_CREATE,
        'task_id' => 1
    ],
    [
        'user_lesson_id' => 4,
        'status' => UserTask::STATUS_CREATE,
        'task_id' => 2
    ],
    [
        'user_lesson_id' => 4,
        'status' => UserTask::STATUS_CREATE,
        'task_id' => 3
    ],
    [
        'user_lesson_id' => 4,
        'status' => UserTask::STATUS_CREATE,
        'task_id' => 4
    ],
    [
        'user_lesson_id' => 4,
        'status' => UserTask::STATUS_CREATE,
        'task_id' => 5
    ],
];
