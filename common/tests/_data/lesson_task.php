<?php

return [
    [
        'lesson_id' => 1,
        'task_id' => 1
    ],
    [
        'lesson_id' => 1,
        'task_id' => 2
    ],
    [
        'lesson_id' => 1,
        'task_id' => 3
    ],
    [
        'lesson_id' => 1,
        'task_id' => 4
    ],
    [
        'lesson_id' => 1,
        'task_id' => 5
    ],
    ////////
    [
        'lesson_id' => 2,
        'task_id' => 1
    ],
    [
        'lesson_id' => 2,
        'task_id' => 2
    ],
    [
        'lesson_id' => 2,
        'task_id' => 3
    ],
    [
        'lesson_id' => 2,
        'task_id' => 4
    ],
    [
        'lesson_id' => 2,
        'task_id' => 5
    ],
    ///////////
    [
        'lesson_id' => 3,
        'task_id' => 1
    ],
    [
        'lesson_id' => 3,
        'task_id' => 2
    ],
    [
        'lesson_id' => 3,
        'task_id' => 3
    ],
    [
        'lesson_id' => 3,
        'task_id' => 4
    ],
    [
        'lesson_id' => 4,
        'task_id' => 5
    ],
    ///////
    [
        'lesson_id' => 3,
        'task_id' => 1
    ],
    [
        'lesson_id' => 3,
        'task_id' => 2
    ],
    [
        'lesson_id' => 3,
        'task_id' => 3
    ],
    [
        'lesson_id' => 4,
        'task_id' => 4
    ],
    [
        'lesson_id' => 4,
        'task_id' => 5
    ],
];
