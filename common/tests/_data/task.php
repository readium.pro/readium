<?php

use common\models\Task;

return [
    [
        'type_skill' => Task::TYPE_SKILL_ARTICULATION,
        'type_form' => Task::TYPE_FORM_ALPHABET,
        'title' => 'Читать слова на время title',
        'description' => 'Читать слова на время'
    ],
    [
        'type_skill' => Task::TYPE_SKILL_ATTENTION,
        'type_form' => Task::TYPE_FORM_WORD,
        'title' => 'Составить из букв слово title',
        'description' => 'Составить из букв слово'
    ],
    [
        'type_skill' => Task::TYPE_SKILL_MEMORY,
        'type_form' => Task::TYPE_FORM_NUMBER,
        'title' => 'Раставить цифры title',
        'description' => 'Раставить цифры'
    ],
    [
        'type_skill' => Task::TYPE_SKILL_SPEED_OF_THINKING,
        'type_form' => Task::TYPE_FORM_READING_TEST,
        'title' => 'Прочитать ответить на вопросы title',
        'description' => 'Прочитать ответить на вопросы после текста'
    ],
    [
        'type_skill' => Task::TYPE_SKILL_READING_SPEED,
        'type_form' => Task::TYPE_FORM_CHOSE_WORD,
        'title' => 'Какие слова были показаны title',
        'description' => 'Перед тобой на долю секунды появится несколько пар слов, твоя задача запомнить их и выбрать правильные'
    ],
];
