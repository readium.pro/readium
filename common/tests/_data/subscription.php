<?php

return [
    [
        'price' => 200,
        'count_day' => 30,
        'description' => 'Подписка на 30 дней',
    ],
    [
        'price' => 600,
        'count_day' => 90,
        'description' => 'Подписка на 90 дней',
    ],
    [
        'price' => 1200,
        'count_day' => 180,
        'description' => 'Подписка на 180 дней',
    ]
];
