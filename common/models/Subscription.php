<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "subscription".
 *
 * @property int $id
 * @property float $price Цена подписки
 * @property int $count_day Колличесвто дней
 * @property int $description Описание
 * @property string $created_at Дата создания
 *
 * @property SubscriptionInvoice[] $subscriptionInvoices
 */
class Subscription extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subscription';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price', 'count_day', 'description'], 'required'],
            [['price'], 'number'],
            [['count_day', 'description'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price' => 'Price',
            'count_day' => 'Count Day',
            'description' => 'Description',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[SubscriptionInvoices]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptionInvoices()
    {
        return $this->hasMany(SubscriptionInvoice::className(), ['subscription_id' => 'id']);
    }
}
