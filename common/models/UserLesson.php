<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use DateTime;

/**
 * This is the model class for table "user_lesson".
 *
 * @property int $id
 * @property int $user_id id пользователя
 * @property int $lesson_id Номмер лекции
 * @property int $status Статус
 * @property string $created_at Дата добавления пользователю лекции
 * @property string|null $updated_at
 *
 * @property Lesson $lesson
 * @property User $user
 * @property UserTask[] $userTasks
 */
class UserLesson extends \yii\db\ActiveRecord
{
    public const STATUS_CREATE = 1;
    public const STATUS_PROGRESS = 2;
    public const STATUS_COMPLETE = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_lesson';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'lesson_id', 'status'], 'required'],
            [['user_id', 'lesson_id', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [
                ['lesson_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Lesson::className(),
                'targetAttribute' => ['lesson_id' => 'id']
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['user_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'lesson_id' => 'Lesson ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Lesson]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLesson()
    {
        return $this->hasOne(Lesson::className(), ['id' => 'lesson_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUserTasks() : ActiveQuery
    {
        return $this->hasMany(UserTask::class, ['user_lesson_id' => 'id']);
    }

    public function getStatus() : string
    {
        switch ($this->status) {
            case self::STATUS_CREATE:
                return 'Не пройденно';
            case self::STATUS_PROGRESS:
                return 'В процессе';
            case self::STATUS_COMPLETE:
                return 'Пройденно';
            default:
                return 'Статус не задан';
        }
    }
}
