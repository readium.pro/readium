<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "invoice".
 *
 * @property int $id
 * @property int $user_id
 * @property int $status Статус
 * @property float $amount Сумма
 * @property string|null $description Описание
 * @property string $created_at
 * @property string|null $paid_at Дата оплаты
 *
 * @property User $user
 * @property SubscriptionInvoice[] $subscriptionInvoices
 */
class Invoice extends \yii\db\ActiveRecord
{
    public const STATUS_ACCEPTED = 1;
    public const STATUS_SUCCESS = 2;
    public const STATUS_PENDING = 3;
    public const STATUS_FAIL = 4;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'invoice';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'status', 'amount'], 'required'],
            [['user_id', 'status'], 'integer'],
            [['amount'], 'number'],
            [['created_at', 'paid_at'], 'safe'],
            [['description'], 'string', 'max' => 1024],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'status' => 'Status',
            'amount' => 'Amount',
            'description' => 'Description',
            'created_at' => 'Created At',
            'paid_at' => 'Paid At',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Gets query for [[SubscriptionInvoices]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptionInvoices()
    {
        return $this->hasMany(SubscriptionInvoice::className(), ['invoice_id' => 'id']);
    }
}
