<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_task".
 *
 * @property int $id
 * @property int user_lesson_id
 * @property int $task_id id занятия
 * @property int $status Статус
 * @property string $created_at Дата добавления пользователю лекции
 * @property string|null $updated_at
 *
 * @property UserLesson $userLesson
 * @property Task $task
 */
class UserTask extends \yii\db\ActiveRecord
{
    public const STATUS_CREATE = 1;
    public const STATUS_COMPLETE = 2;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_task';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'user_lesson_id', 'task_id', 'status'], 'required'],
            [[ 'user_lesson_id', 'task_id', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_id' => 'id']],
            [['user_lesson_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserLesson::className(), 'targetAttribute' => ['user_lesson_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'user_lesson_id' => 'User Lesson ID',
            'task_id' => 'Task ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Lesson]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserLesson()
    {
        return $this->hasOne(UserLesson::className(), ['id' => 'user_lesson_id']);
    }

    /**
     * Gets query for [[Task]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }
}
