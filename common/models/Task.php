<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property int $type_skill Тип навыка
 * @property int $type_form Тип формы
 * @property string $title Заголовок
 * @property string $description Описание
 *
 * @property LessonTask[] $lessonTasks
 * @property TaskData[] $taskDatas
 * @property UserTask[] $userTasks
 */
class Task extends \yii\db\ActiveRecord
{
    public const TYPE_SKILL_ARTICULATION = 1;//артикуляция
    public const TYPE_SKILL_SPEED_OF_THINKING = 2;//скорость мышления
    public const TYPE_SKILL_MEMORY = 3;//память
    public const TYPE_SKILL_ATTENTION = 4;//внимание
    public const TYPE_SKILL_READING_SPEED = 5;//скорость чтения

    //относится к артикуляция
    public const TYPE_FORM_ALPHABET = 1;

    //относится к скорость мышления
    public const TYPE_FORM_READING_TEST = 1;

    // относится к память
    public const TYPE_FORM_NUMBER = 1;

    // относится к внимание
    public const TYPE_FORM_WORD = 1;

    // относится к скорость чтения
    public const TYPE_FORM_CHOSE_WORD = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type_form','type_skill', 'title', 'description'], 'required'],
            [['type_form','type_skill'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 2048],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_form' => 'Тип навыка',
            'type_skill' => 'Тип формы',
            'title' => 'Title',
            'description' => 'Description',
        ];
    }

    /**
     * Gets query for [[LessonTasks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLessonTasks()
    {
        return $this->hasMany(LessonTask::className(), ['task_id' => 'id']);
    }

    /**
     * Gets query for [[TaskDatas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTaskDatas()
    {
        return $this->hasMany(TaskData::className(), ['task_id' => 'id']);
    }

    /**
     * Gets query for [[UserTasks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserTasks()
    {
        return $this->hasMany(UserTask::className(), ['task_id' => 'id']);
    }
}
