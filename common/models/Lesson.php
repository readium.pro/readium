<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;
/**
 * This is the model class for table "lesson".
 *
 * @property int $id
 * @property int $number
 * @property int $status
 *
 * @property LessonTask[] $lessonTasks
 * @property UserLesson[] $userLessons
 * @property UserTask[] $userTasks
 */
class Lesson extends \yii\db\ActiveRecord
{
    public const STATUS_FREE = 1;
    public const STATUS_PAYMENT = 2;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lesson';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number', 'status'], 'required'],
            [['number', 'status'], 'integer'],
            [['number'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Number',
            'status' => 'Status',
        ];
    }

    /**
     * Gets query for [[LessonTasks]].
     *
     * @return ActiveQuery
     */
    public function getLessonTasks() : ActiveQuery
    {
        return $this->hasMany(LessonTask::class, ['lesson_id' => 'id']);
    }

    /**
     * Gets query for [[UserLessons]].
     *
     * @return ActiveQuery
     */
    public function getUserLessons() : ActiveQuery
    {
        return $this->hasMany(UserLesson::class, ['lesson_id' => 'id']);
    }

    /**
     * Gets query for [[UserTasks]].
     *
     * @return ActiveQuery
     */
    public function getUserTasks() : ActiveQuery
    {
        return $this->hasMany(UserTask::class, ['lesson_id' => 'id']);
    }
}
