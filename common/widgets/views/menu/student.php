<?php

use yii\helpers\Html;

?>
<nav id="sidebar">
    <div class="sidebar-header">
        <h3>Ученик</h3>
    </div>
    <ul class="list-unstyled components">
        <p><?= Yii::$app->user->identity->username ?></p>
        <li class="active">
            <a href="#readSpeed" data-toggle="collapse" aria-expanded="false"
               class="dropdown-toggle">Скорость чтения</a>
            <ul class="collapse list-unstyled" id="readSpeed">
                <li>
                    <?= Html::a('Тест', '/student/read-speed/index'); ?>
                </li>
                <li>
                    <?= Html::a('Статистика', '/student/read-speed/statistic'); ?>
                </li>
            </ul>
        </li>
        <li class="active">
            <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false"
               class="dropdown-toggle">Занятия</a>
            <ul class="collapse list-unstyled" id="homeSubmenu">
                <li>
                    <?= Html::a('Доступные', '/student/lesson/index'); ?>
                </li>
                <li>
                    <?= Html::a('Статистика', '#'); ?>
                </li>
            </ul>
        </li>
        <li>
            <?= Html::a('Выход', '/site/logout') ?>
        </li>
    </ul>
</nav>
