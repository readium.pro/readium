<?php

namespace common\widgets;

use frontend\modules\student\Student;
use yii\bootstrap\Widget;
use Yii;
use common\models\User;

class Menu extends Widget
{
    public function run(): string
    {
        if (!Yii::$app->user->isGuest) {
            /**
             * TODO при расширении может добавится проверка на учитель или/и родитель
             * тут проверка на controller
             * если авторизован студент то выводить меню для студента
             */
            return $this->render('menu/student');
        }

        return '';
    }
}
