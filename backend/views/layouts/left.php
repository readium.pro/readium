<?php
use dmstr\widgets\Menu;
?>
<aside class="main-sidebar">
    <section class="sidebar">
        <?= Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
//                    ['label' => 'Film', 'url' => ['/film'], 'icon' => 'film'],
//                    ['label' => 'Seance', 'url' => ['/seance'], 'icon' => 'far fa-clock'],
//                    ['label' => 'Ticket', 'url' => ['/ticket'], 'icon' => 'fas fa-ticket-alt'],
//                    ['label' => 'Staff', 'url' => ['/staff'], 'icon' => 'fas fa-users'],
//                    ['label' => 'Review', 'url' => ['/review'], 'icon' => 'fas fa-book'],
//                    ['label' => 'Film Staff', 'url' => ['/film-staff'], 'icon' => 'fas fa-address-card'],
//                    ['label' => 'Rating', 'url' => ['/rating'], 'icon' => 'fas fa-star'],
//                    ['label' => 'Hall', 'url' => ['/hall'], 'icon' => 'fas fa-person-booth'],
//                    ['label' => 'Place', 'url' => ['/place'], 'icon' => 'fas fa-couch'],
                    ['label' => 'User', 'url' => ['/user'], 'icon' => 'user'],
                    ['label' => 'Config', 'url' => ['/config'], 'icon' => 'cogs'],
                    ['label' => 'Lessons', 'url' => ['/lesson'], 'icon' => 'chalkboard-teacher'],
                ],
            ]
        ) ?>
    </section>
</aside>
