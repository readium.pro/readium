<?php

namespace frontend\controllers;

use common\models\Subscription;
use common\models\SubscriptionInvoice;
use common\models\User;
use yii\web\BadRequestHttpException;
use Yii;
use common\models\Invoice;
use common\helper\DateTime as HelperDateTime;
use DateTime;

class PaymentController extends DefaultController
{

    public function actionTestInvoice()
    {
        if (!Yii::$app->user->getIsGuest()) {
            // TODO думаю тут лучше будет разделить на методы
            $subscription = Subscription::findOne(Yii::$app->request->get('subscription_id'));
            $invoice = $this->createInvoice($subscription);
            $this->createSubscriptionInvoice($subscription, $invoice);
            $this->updateUserSubscription($subscription);
        } else {
            Yii::$app->session->addFlash('error', 'Авторизуйтесь или зарегистрируйтесь!');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionInvoice()
    {
        $model = new Invoice();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            /** @var \robokassa\Merchant $merchant */
            $merchant = Yii::$app->get('robokassa');

            return $merchant->payment(
                $model->amount,
                $model->id,
                'Пополнение счета',
                null,
                Yii::$app->user->identity->email
            );
        } else {
            return $this->render('invoice', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'result' => [
                'class' => '\robokassa\ResultAction',
                'callback' => [$this, 'resultCallback'],
            ],
            'success' => [
                'class' => '\robokassa\SuccessAction',
                'callback' => [$this, 'successCallback'],
            ],
            'fail' => [
                'class' => '\robokassa\FailAction',
                'callback' => [$this, 'failCallback'],
            ],
        ];
    }

    /**
     * Callback.
     *
     * @param \robokassa\Merchant $merchant merchant.
     * @param integer $nInvId invoice ID.
     * @param float $nOutSum sum.
     * @param array $shp user attributes.
     */
    public function successCallback($merchant, $nInvId, $nOutSum, $shp)
    {
        $this->loadModel($nInvId)->updateAttributes(['status' => Invoice::STATUS_ACCEPTED]);

        return $this->goBack();
    }

    /**
     * @param $merchant
     * @param $nInvId
     * @param $nOutSum
     * @param $shp
     *
     * @return string
     * @throws BadRequestHttpException
     */
    public function resultCallback($merchant, $nInvId, $nOutSum, $shp)
    {
        $this->loadModel($nInvId)->updateAttributes(['status' => Invoice::STATUS_SUCCESS]);

        return 'OK' . $nInvId;
    }

    /**
     * @param $merchant
     * @param $nInvId
     * @param $nOutSum
     * @param $shp
     *
     * @return string
     * @throws BadRequestHttpException
     */
    public function failCallback($merchant, $nInvId, $nOutSum, $shp)
    {
        $model = $this->loadModel($nInvId);
        if ($model->status == Invoice::STATUS_PENDING) {
            $model->updateAttributes(['status' => Invoice::STATUS_FAIL]);

            return 'Ok';
        } else {
            return 'Status has not changed';
        }
    }

    /**
     * @param integer $id
     *
     * @return Invoice
     * @throws BadRequestHttpException
     */
    protected function loadModel($id) : Invoice
    {
        $model = Invoice::findOne($id);
        if ($model === null) {
            throw new BadRequestHttpException;
        }

        return $model;
    }

    /**
     * @param Subscription $subscription
     *
     * @return Invoice
     */
    protected function createInvoice(Subscription $subscription) : Invoice
    {
        $invoice = new Invoice();
        $invoice->amount = $subscription->price;
        $invoice->status = Invoice::STATUS_SUCCESS;
        $invoice->user_id = Yii::$app->user->id;
        if (!$invoice->save()) {
            Yii::$app->session->addFlash('error', implode(',', $invoice->getFirstErrors()));
        } else {
            Yii::$app->session->addFlash('success', 'Подписка успешно преобретенна');
        }

        return $invoice;
    }

    /**
     * @param Subscription $subscription
     * @param Invoice $invoice
     *
     * @return SubscriptionInvoice
     */
    protected function createSubscriptionInvoice(
        Subscription $subscription,
        Invoice $invoice
    ) : SubscriptionInvoice {
        $subscriptionInvoice = new SubscriptionInvoice();
        $subscriptionInvoice->subscription_id = $subscription->id;
        $subscriptionInvoice->invoice_id = $invoice->id;

        if (!$subscriptionInvoice->save()) {
            Yii::$app->session->addFlash('error', implode(',', $subscriptionInvoice->getFirstErrors()));
        }

        return $subscriptionInvoice;
    }

    /**
     * @param Subscription $subscription
     *
     * @return User
     * @throws \Exception
     */
    protected function updateUserSubscription(Subscription $subscription) : User
    {
        $user = User::findOne(Yii::$app->user->id);
        $date = new DateTime();
        if (isset($user->subscription_end_date) && strtotime($user->subscription_end_date) > $date->getTimestamp()) {
            $date = new DateTime($user->subscription_end_date);
        }
        $date->modify(sprintf('+ %s days', $subscription->count_day));
        $user->subscription_end_date = $date->format(HelperDateTime::DATE_FORMAT_MYSQL);
        $user->save();

        return $user;
    }
}
