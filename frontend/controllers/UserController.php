<?php

namespace frontend\controllers;

use common\models\Lesson;
use common\models\LessonTask;
use common\models\User;
use common\models\UserLesson;
use common\models\UserTask;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use Yii;

class UserController extends DefaultController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'view'],
                'rules' => [
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param string $token
     *
     * @return \yii\web\Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionVerification(string $token)
    {
        $user = User::find()->where(['verification_token' => $token])->one();
        /** @var $user User */
        if (isset($user)) {
            $user->status = User::STATUS_ACTIVE;
            $user->verification_token = '';
            $user->update();
            //$this->generateFreeUserLesson($user);
            Yii::$app->session->setFlash('success', 'account activated');
        } else {
            Yii::$app->session->setFlash('error', 'auth!');
        }

        return $this->redirect('/site/login');
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    protected function generateFreeUserLesson(User $user) : self
    {
        $lessons = Lesson::find()
            ->andWhere(['status' => Lesson::STATUS_FREE])
            ->all();
        foreach ($lessons as $lesson) {
            /**
             * @var $lesson Lesson
             */
            $userLesson = new UserLesson();
            $userLesson->user_id = $user->id;
            $userLesson->lesson_id = $lesson->id;
            $userLesson->status = UserLesson::STATUS_CREATE;
            $userLesson->save();
            $this->generateUserTask($lesson, $user);
        }

        return $this;
    }

    /**
     * @param Lesson $lesson
     * @param User $user
     *
     * @return $this
     */
    protected function generateUserTask(Lesson $lesson, User $user) : self
    {
        $lessonTasks = LessonTask::find()
            ->andWhere(['lesson_id' => $lesson->id])
            ->all();
        foreach ($lessonTasks as $lessonTask) {
            /**
             * @var $lessonTask LessonTask
             */
            $userTask = new UserTask();
            $userTask->task_id = $lessonTask->task_id;
            $userTask->user_lesson_id = $lessonTask->id;
            $userTask->status = UserTask::STATUS_CREATE;
            $userTask->save();
        }

        return $this;
    }
}
