<?php

namespace frontend\controllers;

use yii\web\Controller;
use common\models\User;
use yii\helpers\Url;
use Yii;

class DefaultController extends Controller
{
    protected $actionIgnore;

    /**
     * @return array
     */
    public function getActionIgnore() : array
    {
        return $this->actionIgnore;
    }

    /**
     * @param array $param
     *
     * @return array
     */
    public function setActionIgnore(array $param) : array
    {
        return $this->actionIgnore = $param;
    }
}