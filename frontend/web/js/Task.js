class Task {
    task_id;
    user_lesson_id;
    /**
     * @type {Object}
     */
    data;
    /**
     * @type {string}
     */
    description;

    /**
     * @param {TaskData} taskData
     */
    constructor(taskData) {
        this.task_id = taskData.task_id;
        this.user_lesson_id = taskData.user_lesson_id;
        this.data = taskData.data;
        this.description = taskData.description;
    }

    render() {
        this.descriptionRender();
        this.initDescriptionEvent();
    }

    renderData() {
        Task.getForm().html('');
        let html = '<h1>Пока не готово ' + this.constructor.name + ' method renderData</h1>';
        html +='<div class="btn btn-success next_task btn-center">Прочитал</div>'
        Task.getForm().html(html);
        //console.error('Override method renderData class ' + this.constructor.name)
    }

    /**
     * @returns {number}
     */
    static getTypeSkill() {
        let self = this;
        console.error('Override static method getTypeSkill class ' + self.constructor.name)
    }

    /**
     * @returns {number}
     */
    static getTypeForm() {
        let self = this;
        console.error('Override static method getTypeForm class ' + self.constructor.name)
    }

    /**
     * @return {*|Window.jQuery|HTMLElement}
     */
    static getForm() {
        return $('#user-lesson-view');
    }

    descriptionRender() {
        Task.getForm().html('');
        let html =
            '<div class="description col-xs-12 text-center">' + this.description + '</div>' +
            '<div class="btn btn-success start_task btn-center">Старт</div>'
        Task.getForm().html(html);
    }

    initDescriptionEvent() {
        let self = this;
        $('.start_task').on('click', function () {
            self.renderData();
        })
    }
    renderNextTaskButton() {
        let text = 'Перейти к следующему занятию';
        let textBtn = 'Перейти';
        if(yii.lesson.tasks.length === yii.lesson.numberTask){
            text = 'Поздравляю выпрошли лекцию номер ' +  yii.getQueryParams(document.location.href).number;
            textBtn = 'Посмотрть доступные лекции';
        }
        Task.getForm().html('');
        let html = '<h1 class="text-center">' + text + '</h1>' +
            '<div class="btn btn-success next_task btn-center">' + textBtn + '</div>'
        Task.getForm().html(html);
    }
}
