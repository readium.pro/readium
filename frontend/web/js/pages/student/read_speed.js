$(document).ready(function () {
    let start, end, result;
    $('.start_read').click(function () {
        $('.start_read').addClass('hidden');
        $('.read_speed_text').removeClass('hidden');
        $('.end_read').removeClass('hidden');
        start = new Date();
    });
    $('.end_read').click(function () {
        $('.read_speed_text').addClass('hidden');
        $('.end_read').addClass('hidden');
        end = new Date();
        result = parseInt(((end - start) / 1000));
        let countWords = $('.read_speed_text').text().split(" ").length - 1;
        let wordPerMinute = (countWords / result) * 60;
        $.ajax({
            type: 'POST',
            url:'/student/read-speed/save',
            data: {
                _csrf: yii.getCsrfToken(),
                wordPerMinute: wordPerMinute,
            },
            success: function (param) {
                console.log(param)
                console.log('success');
                if (confirm('Вашь результат : ' + wordPerMinute + ' слов в минуту!Попробовать еще раз?')) {
                    // Save it!
                    document.location.reload();
                } else {
                    // Do nothing!
                    document.location.href = document.location.origin + '/student/lesson/index';
                }
            },
            error: function (param) {
                console.log(param)
                console.log('error')
            }
        })
    });
});
