$(document).ready(function () {
    yii.lesson = new Lesson(tasks);
    yii.lesson.renderNextTask();
    $('body').on('click','.next_task',function () {
        /**
         * TODO нужно добавить тут будет
         * 1) проверку если нужна на правильность выполнения урока
         * 2) ajax запрос чтоб данные об этом задании обновились
         */
        yii.lesson.updateStatus().done(function () {
            yii.lesson.renderNextTask();
        })
    })
});
