class Numbers extends Memory {
    index = 0;

    /**
     * @returns {number}
     */
    static getTypeForm() {
        return 1;
    }

    /**
     * @param {TaskData} taskData
     */
    constructor(taskData) {
        taskData.data = taskData.data[0];
        super(taskData);
    }

    renderData() {
        Task.getForm().html('');
        let html = '' +
            '<h2 class="view_number opacity-0 text-center">' + this.getNumber() + '</h2>' +
            '<div class="m-0-a display-table"><input type="text" class="number"/></div>' +
            '<div class="btn btn-success check_number btn-center">Проверить</div>'
        Task.getForm().html(html);
    }

    render() {
        super.render();
        this.initEvent();
    }

    initEvent() {
        let self = this;
        $('body').on('click', '.check_number', function () {
            if ($('.number').val() == self.getNumber()) {
                self.index++;
                if (self.index == self.data.length) {
                    self.renderNextTaskButton();
                } else {
                    alert('Правильно!');
                    self.renderData();
                    self.eventViewNumbers();
                }
            } else {
                alert('Попробуйте еще раз!');
                self.renderData();
                self.eventViewNumbers();
            }
        });
    }

    /**
     * @returns {string}
     */
    getNumber() {
        return this.data[this.index].join('');
    }

    eventViewNumbers() {
        $('.view_number').removeClass('opacity-0');
        let timerId = setTimeout(function tick() {
            $('.view_number').addClass('opacity-0');
            clearInterval(timerId);
        }, 3000);
    }

    initDescriptionEvent() {
        let self = this;
        $('.start_task').on('click', function () {
            self.renderData();
            self.eventViewNumbers();
        })
    }
}
