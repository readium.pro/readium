class ChooseWord extends ReadingSpeed {
    /**
     * @returns {number}
     */
    static getTypeForm() {
        return 1;
    }

    render() {
        super.render();
        this.initEvent();
    }

    initEvent() {
        let self = this;
        $('body').on('click', '.check_question_choose_word', function () {
            if (self.checkQuestion()) {
                self.renderNextTaskButton();
            } else {
                alert('Неверно попробуйте еще раз!');
            }
        });
    }

    renderData() {
        Task.getForm().html('');
        let info = this.getData().options;
        let html = '<h1 class="view_words">' + this.getWords().join(' ') + '</h1>';
        html += '<div class="question_word opacity-0"><div>' + this.getData().question + '</div>';

        for (let i = 0; i < info.length; i++) {
            html +=
                '<div>' + info[i] +
                ' <input type="checkbox" name="question_radio" class="question_radio" value="' + info[i] + '" />' +
                '</div>'
        }
        html += '<div class="btn btn-success check_question_choose_word">Ответить</div></div>';
        Task.getForm().html(html);
    }

    getWords() {
        return this.getData().options_true
    }

    /**
     * @returns {ChooseWordData}
     */
    getData() {
        return this.data;
    }

    eventViewWords() {
        $('.view_words').removeClass('opacity-0');
        let timerId = setTimeout(function tick() {
            $('.view_words').addClass('opacity-0');
            $('.question_word').removeClass('opacity-0');
            clearInterval(timerId);
        }, 3000);
    }

    initDescriptionEvent() {
        let self = this;
        $('.start_task').on('click', function () {
            self.renderData();
            self.eventViewWords();
        })
    }

    checkQuestion() {
        let items = $('.question_radio');
        let checkedItem = [];
        for (let i = 0; i < items.length; i++) {
            if ($(items[i]).is(':checked')) {
                checkedItem.push($(items[i]).val());
            }
        }
        return checkedItem.join('') === this.getData().options_true.join('');
    }
}

class ChooseWordData {
    question;
    options;
    options_true;
}
