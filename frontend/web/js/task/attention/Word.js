class Word extends Attention {
    number = 0;

    /**
     * @returns {number}
     */
    static getTypeForm() {
        return 1;
    }

    renderData() {
        Task.getForm().html('');
        let html = '<h1 class="text-center">' + this.getRandCharWord() + '</h1>' +
            '<div class="m-0-a display-table"><input type="text" class="word"/></div>' +
            '<div class="btn btn-success check_word btn-center">Проверить</div>'
        Task.getForm().html(html);
    }

    render() {
        super.render();
        this.initEvent();
    }

    initEvent() {
        let self = this;
        $('body').on('click', '.check_word', function () {
            if ($('.word').val() == self.getWord()) {
                self.number++;
                if (self.number == self.data.length) {
                    self.renderNextTaskButton();
                } else {
                    alert('Правильно!');
                    self.renderData();
                }
            } else {
                alert('Попробуйте еще раз!');
                self.renderData();
            }
        })
    }

    /**
     * @returns {string}
     */
    getWord() {
        return this.data[this.number];
    }

    /**
     * @returns {string}
     */
    getRandCharWord() {
        let word = this.getWord();
        let a = word.split(""),
            n = a.length;

        for (let i = n - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1));
            let tmp = a[i];
            a[i] = a[j];
            a[j] = tmp;
        }
        return a.join("") === this.getWord() ? this.getRandCharWord() : a.join("");
    }
}
