class Alphabet extends Articulation {
    /**
     * @returns {number}
     */
    static getTypeForm() {
        return 1;
    }

    renderData() {
        Task.getForm().html('');
        let html = '<table class="alphabet"><tr>';
        for (let i = 0; i < this.data.length; i++) {
            if (i % 5 == 0 && i != 0) {
                html += '</tr><tr>';
            }
            html += '<td>' + this.data[i] + '</td>';
        }
        html += '</tr></table>';
        html += '<div class="btn btn-success read btn-center">Прочитал</div>'
        Task.getForm().html(html);
    }

    render() {
        super.render();
        this.initEvent();
    }

    initEvent() {
        let self = this;
        $('body').on('click', '.read', function () {
            self.renderNextTaskButton();
        })
    }
}
