class SpeedOfThinking extends Task {
    /**
     * @returns {number}
     */
    static getTypeSkill() {
        return 2;
    }
}
