class ReadingTest extends SpeedOfThinking {
    /**
     * @returns {number}
     */
    static getTypeForm() {
        return 1;
    }

    /**
     * @returns {ReadingTestData}
     */
    getData() {
        return this.data;
    }

    renderData() {
        Task.getForm().html('');
        let html = '<div>' + this.getData().text + '</div>' +
            '<div class="btn btn-success question">Прочитал</div>'
        Task.getForm().html(html);
    }

    render() {
        super.render();
        this.initEvent();
    }

    initEvent() {
        let self = this;
        $('body').on('click', '.question', function () {
            self.renderQuestion();
        });
        $('body').on('click', '.check_question_reading_test', function () {
            if (self.checkQuestion()) {
                self.renderNextTaskButton();
            } else {
                alert('Не верно попробуйте еше раз!')
            }
        });
    }

    renderQuestion() {
        Task.getForm().html('');
        let html = '<div>' + this.getData().question + '</div>';
        let info = this.getData().options
        for (let i = 0; i < info.length; i++) {
            html +=
                '<div>' + info[i] +
                ' <input type="radio" name="question_radio" class="question_radio" value="' + info[i] + '" />' +
                '</div>'
        }
        html += '<div class="btn btn-success check_question_reading_test">Ответить</div>';
        Task.getForm().html(html);
    }

    checkQuestion() {
        let items = $('.question_radio');
        for (let i = 0; i < items.length; i++) {
            if ($(items[i]).is(':checked') && $(items[i]).val() == this.getData().option_true) {
                return true;
            }
        }
        return false;
    }
}

class ReadingTestData {
    text;
    question;
    option_true;
    options;
}
