class Lesson {
    tasks = [];
    numberTask = 0;

    /**
     * @param {array} tasks
     */
    constructor(tasks) {
        this.loadTasks(tasks);
    }

    renderNextTask() {
        if (this.tasks.length === this.numberTask) {
            document.location.href = '/student/lesson/index';
        } else {
            /** @type {Task} */
            let task = this.tasks[this.numberTask];
            task.render();
            this.numberTask++;
            /**
             * TODO
             * 1) нужен редирект на страницу с уроками (лекциями)
             * 2) нужен ajax запрос для обновления статуса лекции
             */
        }
    }

    /**
     *
     * @param {array} tasks
     */
    loadTasks(tasks) {
        for (let i = 0; i < tasks.length; i++) {
            this.tasks[i] = this.getTaskModel(tasks[i]);
        }
    }

    /**
     * @param {TaskData} task
     * @return {Task}
     */
    getTaskModel(task) {
        switch (parseInt(task.type_skill)) {
            //артикуляция
            case Articulation.getTypeSkill():
                return this.getArticulateModel(task);
            //скорость мышления
            case SpeedOfThinking.getTypeSkill():
                return this.getSpeedOfThinking(task);
            //память
            case Memory.getTypeSkill():
                return this.getMemoryModel(task);
            //внимание
            case Attention.getTypeSkill():
                return this.getAttentionModel(task);
            //скорость чтения
            case ReadingSpeed.getTypeSkill():
                return this.getReadingSpeedModel(task);
            default:
                console.log(task.type_skill, Articulation.getTypeSkill())
                throw new TypeError('type_skill not found')
        }
    }

    /**
     * @param {TaskData} task
     * @return {Articulation}
     */
    getArticulateModel(task) {
        switch (parseInt(task.type_form)) {
            case Alphabet.getTypeForm():
                return new Alphabet(task)
            default:
                throw new TypeError('type_form not found')
        }
    }

    /**
     * @param {TaskData} task
     * @return {Attention}
     */
    getAttentionModel(task) {
        switch (parseInt(task.type_form)) {
            case Word.getTypeForm():
                return new Word(task)
            default:
                throw new TypeError('type_form not found')
        }
    }

    /**
     * @param {TaskData} task
     * @return {Memory}
     */
    getMemoryModel(task) {
        switch (parseInt(task.type_form)) {
            case Numbers.getTypeForm():
                return new Numbers(task)
            default:
                throw new TypeError('type_form not found')
        }
    }

    /**
     * @param {TaskData} task
     * @return {ReadingSpeed}
     */
    getReadingSpeedModel(task) {
        switch (parseInt(task.type_form)) {
            case ChooseWord.getTypeForm():
                return new ChooseWord(task)
            default:
                throw new TypeError('type_form not found')
        }
    }

    /**
     * @param {TaskData} task
     * @return {SpeedOfThinking}
     */
    getSpeedOfThinking(task) {
        switch (parseInt(task.type_form)) {
            case ReadingTest.getTypeForm():
                return new ReadingTest(task)
            default:
                throw new TypeError('type_form not found')
        }
    }

    /**
     * обновляет статус user_task ,user_lesson
     * рендерит следующее задание
     */
    updateStatus() {
        let self = this;
        /**
         * @param {TaskData} taskData
         */
        let taskData = self.tasks[self.numberTask - 1];
        return $.ajax({
            type: 'POST',
            cache: false,
            url: '/student/user-task/update-status',
            data: {
                _csrf: yii.getCsrfToken(),
                taskId: taskData.task_id,
                userLessonId: taskData.user_lesson_id
            },
        })
    }
}
