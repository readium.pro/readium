<?php

namespace frontend\modules\student\controllers;

use common\models\ReadSpeed;
use common\models\UserReadSpeed;
use Yii;
use yii\filters\VerbFilter;

class ReadSpeedController extends DefaultController
{
    /**
     * @return string
     */
    public function actionIndex() : string
    {
        return $this->render('index', [
            'readSpeed' => ReadSpeed::find()->orderBy('rand()')->one()
        ]);
    }

    /**
     * @return string
     */
    public function actionStatistic() : string
    {
        return $this->render('statistic', [
            'userReadSpeeds' => UserReadSpeed::findAll(['user_id' => Yii::$app->user->id])
        ]);
    }

    /**
     * @return bool
     */
    public function actionSave() : bool
    {
        $userReadSpeed = new UserReadSpeed();
        $userReadSpeed->words_per_minute = Yii::$app->request->post('wordPerMinute') ?? null;
        $userReadSpeed->user_id = Yii::$app->user->id;

        return $userReadSpeed->save();
    }
}
