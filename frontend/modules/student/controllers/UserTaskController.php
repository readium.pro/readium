<?php

namespace frontend\modules\student\controllers;

use common\models\UserLesson;
use common\models\UserTask;
use Yii;

class UserTaskController extends DefaultController
{
    public function actionUpdateStatus()
    {
        if (Yii::$app->request->isAjax) {
            $userTask = UserTask::findOne([
                'user_lesson_id' => Yii::$app->request->post('userLessonId'),
                'task_id' => Yii::$app->request->post('taskId')
            ]);
            if (isset($userTask) && $userTask->userLesson->user_id == Yii::$app->user->id) {
                $userTask->status = UserTask::STATUS_COMPLETE;
                $userTask->save();
                $this->updateUserLessonStatus($userTask->userLesson);
            }
        }
    }

    /**
     * Проверяет нужно ли обновить и обновляет если нужно
     *
     * @param UserLesson $userLesson
     */
    protected function updateUserLessonStatus(UserLesson $userLesson)
    {
        $countComplete = 0;
        $userTasks = $userLesson->userTasks;
        foreach ($userTasks as $userTaskItem) {
            if ($userTaskItem->status == UserTask::STATUS_COMPLETE) {
                $countComplete++;
            }
        }
        if ($countComplete == count($userTasks)) {
            $userLesson->status = UserLesson::STATUS_COMPLETE;
        } elseif ($countComplete > 0) {
            $userLesson->status = UserLesson::STATUS_PROGRESS;
        }
        $userLesson->save();
    }
}
