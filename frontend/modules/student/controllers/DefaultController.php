<?php

namespace frontend\modules\student\controllers;

use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Default controller for the `Student` module
 */
class DefaultController extends Controller
{
    public function behaviors() : array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
}
