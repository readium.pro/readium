<?php

namespace frontend\modules\student\controllers;

use common\models\Task;
use common\models\User;
use frontend\modules\student\model\search\LessonIndex;
use common\models\UserLesson;
use common\models\UserTask;
use common\models\Lesson as CommonLesson;
use Yii;
use yii\web\BadRequestHttpException;
use frontend\modules\student\helper\Lesson as HelperLesson;
use yii\web\Response;

class LessonController extends DefaultController
{
    public function actionIndex() : string
    {
        $lesson = new LessonIndex();

        return $this->render('index', [
            'lessons' => $lesson->getAll(),
            'user' => User::findOne(['id' => Yii::$app->user->id])
        ]);
    }

    /**
     * @param int $number
     *
     * @return string|\yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionView(int $number)
    {
        $userLesson = $this->getUserLesson($number);
        /**
         * @var $userLesson UserLesson
         */
        $previousUserLesson = $this->getUserLesson(($number - 1));
        if (!isset($userLesson) && $number > 1 && isset($previousUserLesson) &&
            $previousUserLesson->status != UserLesson::STATUS_COMPLETE) {
            Yii::$app->session->setFlash('error', 'Пройдите предыдущую лекцию!');

            return $this->redirectStudentLessonPage();
        }

        if (!isset($userLesson)) {
            $userLesson = $this->createUserLesson($number);
        }

        if (!HelperLesson::isSubscription($userLesson->user, $userLesson->lesson)) {
            Yii::$app->session->setFlash('error', 'Купите подписку!');

            return $this->redirectStudentLessonPage();
        }

        return $this->render('view', [
            'model' => $userLesson,
            'tasks' => json_encode($this->getTasks($userLesson))
        ]);
    }

    /**
     * @param UserLesson $userLesson
     *
     * @return array
     */
    public function getTasks(UserLesson $userLesson) : array
    {
        /**
         * 1. Проверить если user_task
         * 2. если нету создать
         * 3. Проверить какие задания из лекции уже пройденны
         */
        if (empty($userLesson->userTasks)) {
            $this->createUserTask($userLesson);
        }
        $query = Task::find()
            ->select([
                't.id as task_id',
                't.description',
                't.type_skill',
                't.type_form',
                'td.data',
                'ut.user_lesson_id as user_lesson_id'
            ])
            ->alias('t')
            ->leftJoin('user_task ut', 'ut.task_id = t.id')
            ->leftJoin('(select id,task_id,data from task_data  order by rand()) td ', 'td.task_id = t.id')
            ->andWhere(['ut.user_lesson_id' => $userLesson->id]);
        if ($userLesson->status == UserLesson::STATUS_PROGRESS) {
            $query->andWhere(['ut.status' => UserTask::STATUS_CREATE]);
        }
        $tasks = $query->groupBy('t.id')
            ->asArray()
            ->all();

        array_walk($tasks, function (&$task) {
            $task['data'] = unserialize($task['data']);
        });

        return $tasks;
    }

    /**
     * @param int $number
     *
     * @return CommonLesson
     * @throws BadRequestHttpException
     */
    protected function loadCommonLesson(int $number) : CommonLesson
    {
        $model = CommonLesson::findOne($number);
        if ($model === null) {
            throw new BadRequestHttpException();
        }

        return $model;
    }

    /**
     * @param int $number
     *
     * @return UserLesson
     * @throws BadRequestHttpException
     */
    protected function createUserLesson(int $number) : UserLesson
    {
        $userLesson = new UserLesson();
        $userLesson->user_id = Yii::$app->user->id;
        $userLesson->lesson_id = $this->loadCommonLesson($number)->id;
        $userLesson->status = UserLesson::STATUS_CREATE;
        $userLesson->save();

        return $userLesson;
    }

    /**
     * @param UserLesson $userLesson
     */
    protected function createUserTask(UserLesson $userLesson) : void
    {
        $lessonTasks = $userLesson->lesson->lessonTasks;
        foreach ($lessonTasks as $lessonTask) {
            $userTask = new UserTask();
            $userTask->user_lesson_id = $userLesson->id;
            $userTask->task_id = $lessonTask->task_id;
            $userTask->status = UserTask::STATUS_CREATE;
            $userTask->save();
        }
    }

    /**
     * @param int $number
     *
     * @return UserLesson|null
     */
    protected function getUserLesson(int $number) : ?UserLesson
    {
        return UserLesson::find()
            ->alias('ul')
            ->innerJoinWith('lesson')
            ->andWhere(['lesson.number' => $number])
            ->andWhere(['ul.user_id' => Yii::$app->user->id])
            ->one();
    }

    /**
     * @return Response
     */
    protected function redirectStudentLessonPage() : Response
    {
        return $this->redirect('/student/lesson/index');
    }
}
