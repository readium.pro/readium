<?php

namespace frontend\modules\student\helper;

use common\models\User;
use common\models\UserLesson;
use frontend\modules\student\model\Lesson as ModelLesson;
use common\models\Lesson as CommonLesson;
use DateTime;

class Lesson
{
    public static function isViewLesson(ModelLesson $lesson, bool &$isView, ?int &$previousModelStatus) : void
    {
        if ((!$isView) ||
            (($previousModelStatus == UserLesson::STATUS_COMPLETE || $previousModelStatus == null) &&
                $lesson->statusView == UserLesson::STATUS_CREATE) ||
            ($lesson->statusView == UserLesson::STATUS_PROGRESS) ||
            (empty($lesson->statusView))
        ) {
            $isView = false;
        }
        $previousModelStatus = $lesson->statusView;
    }

    public static function isSubscription(User $user, CommonLesson $lesson) : bool
    {
        $date = new DateTime();
        if ($lesson->status == CommonLesson::STATUS_PAYMENT) {
            if ($user->subscription_end_date === null || (isset($user->subscription_end_date) &&
                    (strtotime($user->subscription_end_date) < $date->getTimestamp()))) {
                return false;
            }
        }

        return true;
    }
}