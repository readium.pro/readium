<?php

namespace frontend\modules\student\model\search;

use common\models\Lesson as CommonLesson;
use common\models\User;
use common\models\UserLesson;
use DateTime;
use Yii;

/**
 * Class Lesson
 * @package frontend\modules\student\model
 */
class LessonIndex extends \common\models\Lesson
{
    public $statusView;//смотрет статусы UserLesson , может быть null
    public $isView;
    public $isSubscription;
    /**
     * @var User|null
     */
    protected $user;

    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->user = User::findOne(Yii::$app->user->id);
    }

    public static function getAll() : array
    {
        $lessons = self::find()
            ->alias('l')
            ->innerJoin('user_lesson ul', 'ul.lesson_id = l.id')
            ->select([
                'l.number as number',
                'l.status as status',
                'ul.status as statusView'
            ])
            ->andWhere(['ul.user_id' => Yii::$app->user->id])
            ->all();
        $isView = true;
        $previousModelStatus = null;
        foreach ($lessons as $lesson) {
            /**
             * @var self $lesson
             */
            $lesson->isView = $isView = $lesson->getIsView($isView,$previousModelStatus);
            $previousModelStatus = $lesson->statusView;
            $lesson->calculateIsSubscription();
        }

        return $lessons;
    }

    private function getIsView(bool $isView, ?int $previousModelStatus) : bool
    {
        if ((!$isView) ||
            (($previousModelStatus == UserLesson::STATUS_COMPLETE || $previousModelStatus == null) &&
                $this->statusView == UserLesson::STATUS_CREATE) ||
            ($this->statusView == UserLesson::STATUS_PROGRESS) ||
            (empty($this->statusView))
        ) {
            return false;
        }

        return true;
    }

    private function calculateIsSubscription() : void
    {
        $date = new DateTime();
        if ($this->status == CommonLesson::STATUS_PAYMENT) {
            if ($this->user->subscription_end_date === null || (isset($this->user->subscription_end_date) &&
                    (strtotime($this->user->subscription_end_date) < $date->getTimestamp()))) {
                $this->isSubscription = false;
            }
        }

        $this->isSubscription = true;
    }
}
