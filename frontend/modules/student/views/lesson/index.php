<?php

use frontend\modules\student\model\search\LessonIndex;
use yii\helpers\Html;
use frontend\assets\LessonIndexAsset;
use common\models\User;

/**
 * @var $lessons \frontend\modules\student\model\search\LessonIndex[]
 * @var $user User
 */
LessonIndexAsset::register($this);
?>
<?php foreach ($lessons as $lesson) : ?>
    <div class="col-sm-4 col-4">
        <div class="text-center
        <?= $lesson->isView === true ? ' lesson-icon-active ' : '' ?>
        <?= $lesson->isView && $lesson->isSubscription ? '' : ' op-0-5 ' ?> lesson-icon">
            <?php if ($lesson->isView === true): ?>
                <?= Html::a(
                    $lesson->number,
                    ['/student/lesson/view', 'number' => $lesson->number],
                    ['class' => 'text-center']
                ) ?>
            <?php endif; ?>
        </div>
    </div>
<?php endforeach; ?>
