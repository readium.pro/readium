<?php

use frontend\assets\LessonViewAsset;

/* @var $this yii\web\View */
/* @var $model common\models\UserLesson */
/* @var $tasks string */
LessonViewAsset::register($this);
?>
<script>
    let tasks = <?= $tasks ?>
</script>
<div id="user-lesson-view"></div>
