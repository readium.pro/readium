<?php

use common\models\ReadSpeed;
use yii\web\View;
use frontend\assets\AppAsset;

/**
 * @var $readSpeed ReadSpeed
 * @var $this View
 */
$this->registerJsFile(
        '/js/pages/student/read_speed.js',
        ['depends' => (new AppAsset())->depends]
);
?>
<div class="start_read btn btn-success">Начать</div>
<div class="read_speed_text hidden"><?= $readSpeed->text ?></div>
<div class="end_read btn btn-success hidden">Прочитал</div>
