<?php

use common\models\UserReadSpeed;

/**
 * @var $userReadSpeeds UserReadSpeed[]
 */
?>
<table class="table">
    <thead>
        <tr>
            <th scope="col">Слов в минуту :</th>
            <th scope="col">Дата :</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($userReadSpeeds as $userReadSpeed): ?>
        <tr>
            <td><?= $userReadSpeed->words_per_minute ?></td>
            <td><?= $userReadSpeed->created_at ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>