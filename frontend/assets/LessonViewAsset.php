<?php

namespace frontend\assets;

class LessonViewAsset extends LessonIndexAsset
{
    public $js = [
        'js/Task.js',
        'js/task/Articulation.js',
        'js/task/Attention.js',
        'js/task/Memory.js',
        'js/task/ReadingSpeed.js',
        'js/task/SpeedOfThinking.js',
        'js/task/articulation/Alphabet.js',
        'js/task/attention/Word.js',
        'js/task/memory/Numbers.js',
        'js/task/reading_speed/ChooseWord.js',
        'js/task/speed_of_thinking/ReadingTest.js',
        'js/Lesson.js',
        'js/TaskData.js',
        'js/libs/jquery-sortable.js',
        'js/pages/lesson/view.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
