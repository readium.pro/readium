<?php

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

NavBar::begin([
    'brandLabel' => Yii::$app->name,
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);

$menuItems[] = ['label' => 'Главная', 'url' => ['/site/index']];

if (Yii::$app->user->isGuest) {
    $menuItems[] = ['label' => 'Вход', 'url' => ['/site/login']];
    $menuItems[] = ['label' => 'Регистрация', 'url' => ['/site/signup']];
} else {
    $menuItems[] = [
        'label' => 'Мой аккаунт (' . Yii::$app->user->identity->username . ')',
        'url' => ['/student/lesson/index'],

    ];
    $menuItems[] = [
        'label' => 'Выход',
        'url' => ['/site/logout'],
        'linkOptions' => ['data-method' => 'post']
    ];
}
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => $menuItems
]);
NavBar::end();
