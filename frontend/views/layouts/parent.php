<?php

/**
 * @var $this \yii\web\View
 * @var $content string
 */

use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use frontend\assets\UserAsset;

UserAsset::register($this);
?>
<?= $this->render('head'); ?>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <?= $this->render('header'); ?>
    <div class="container">
        <!-- Sidebar -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Parent</h3>
            </div>
            <ul class="list-unstyled components">
                <p><?= Yii::$app->user->identity->username ?></p>
                <li>
                    <a href="#">Посмотреть успеваемость</a>
                </li>
                <li>
                    <a href="#">Выход</a>
                </li>
            </ul>
        </nav>
        <div class="content_page">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>
</div>

<?= $this->render('footer') ?>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
