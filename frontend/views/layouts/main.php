<?php

/**
 * @var $this \yii\web\View
 * @var $content string
 */

use yii\widgets\Breadcrumbs;
use common\widgets\Menu;
use common\widgets\Alert;
use frontend\assets\AppAsset;

AppAsset::register($this);
?>
<?= $this->render('head'); ?>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <?= $this->render('header'); ?>
    <div class="container">
        <?= Menu::widget(); ?>
        <?= Alert::widget() ?>
        <div class="content_page">
            <?= $content ?>
        </div>
    </div>
</div>
</div>

<?= $this->render('footer') ?>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
