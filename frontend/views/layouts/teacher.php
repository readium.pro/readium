<?php

/**
 * @var $this \yii\web\View
 * @var $content string
 */

use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use frontend\assets\UserAsset;

UserAsset::register($this);
?>
<?= $this->render('head'); ?>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <?= $this->render('header'); ?>
    <div class="container">
        <!-- Sidebar -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Teacher</h3>
            </div>
            <ul class="list-unstyled components">
                <p><?= Yii::$app->user->identity->username ?></p>
                <li class="active">
                    <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false"
                       class="dropdown-toggle">Классы</a>
                    <ul class="collapse list-unstyled" id="homeSubmenu">
                        <li>
                            <a href="#">Создать класс</a>
                        </li>
                        <li>
                            <a href="#">Список классов</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">Добавить ученика</a>
                </li>
                <li>
                    <a href="#">Выход</a>
                </li>
            </ul>
        </nav>
        <div class="content_page">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>
</div>

<?= $this->render('footer') ?>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
