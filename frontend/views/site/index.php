<?php

use common\models\Subscription;
use yii\widgets\DetailView;
use yii\bootstrap\Html;

/**
 * @var $subscriptions Subscription[]
 */
?>
<h1>Главная страница</h1>
<?php foreach ($subscriptions as $subscription): ?>
    <div class="col-4 col-xs-4 ">
        <p><?= $subscription->description ?></p>
        <?= Html::a('Купить', [
            '/payment/test-invoice',
            'subscription_id' => $subscription->id
        ], ['class' => 'btn btn-success']) ?>
    </div>
<?php endforeach; ?>

